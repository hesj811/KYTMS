package com.kytms.zone.dao;

import com.kytms.core.dao.BaseDao;
import java.util.List;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2017-11-23
 */
public interface ZoneDao<Zone> extends BaseDao<Zone> {
    List<Zone> selectAllZone();
}
